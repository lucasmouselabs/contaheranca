class Conta {
	// Atributos
	protected double saldo;

	// Métodos 
	public void setSaldo ( double umSaldo ){
		this.saldo = umSaldo;
	}

	public double getSaldo (){
		return this.saldo;
	}

	public void deposita (double umValor){
		this.saldo = this.saldo + umValor;
	}

	public void saca (double umValor) {
		this.saldo = this.saldo -umValor;
	}

	public void atualiza (double taxa){
		this.saldo += this.saldo * taxa;
	}	

	
}
